﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Volunteer_Iowa_Site.Models;
using Volunteer_Iowa_Site.DAL;
using Volunteer_Iowa_Site.Filters;

namespace Volunteer_Iowa_Site.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class LogController : Controller
    {
        private LogContext db = new LogContext();
        private GenericFunctions gen = new GenericFunctions();
        //
        // GET: /Log/

        public ActionResult Index()
        {
            var log = db.Log.Include(l => l.Event).Include(l => l.Orginization).Include(l => l.UserProfile);
            int LoggedinUserId  = gen.GetUserID();

            IQueryable<Log> custQuery =
                from id in db.Log
                where id.userId == LoggedinUserId
                select id;

            int totalHours = 0;
            if (custQuery.Any())
            {
                foreach (var userLog in custQuery.ToList())
                {
                    totalHours = totalHours + userLog.hoursLogged;
                }
            }
            ViewBag.logHours = totalHours;

            double percents = ((double)totalHours / (double)50) * 100;
            ViewBag.percent = percents;
            //return View(log.ToList());
            return View(custQuery.ToList());
        }

        public ActionResult All()
        {
            int totalHours = 0;

            foreach (var userLog in db.Log.ToList())
            {
                totalHours = totalHours + userLog.hoursLogged;
            }
            
            ViewBag.logHours = totalHours;

            return View(db.Log.ToList());
        }


        //
        // GET: /Log/Details/5

        public ActionResult Details(int id = 0)
        {
            Log log = db.Log.Find(id);
            if (log == null)
            {
                return HttpNotFound();
            }
            return View(log);
        }

        //
        // GET: /Log/Create

        public ActionResult Create()
        {
            ViewBag.eventID = new SelectList(db.Event, "eventID", "Name");
            ViewBag.orgID = new SelectList(db.Org, "orgID", "orgName");
            //ViewBag.userId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Log/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Log log)
        {
            if (ModelState.IsValid)
            {
                /*
                if (log.logID == 0 || db.Log.Find(log.logID) != null)
                {
                    int count = db.Log.Count<Logs>();
                
                    log.logID = count++;
                }*/
                log.userId = gen.GetUserID();

                db.Log.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.eventID = new SelectList(db.Event, "eventID", "Name", log.eventID);
            ViewBag.orgID = new SelectList(db.Org, "orgID", "orgName", log.orgID);
            ViewBag.userId = new SelectList(db.UserProfiles, "UserId", "UserName", log.userId);
            return View(log);
        }

        //
        // GET: /Log/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Log log = db.Log.Find(id);
            if (log == null)
            {
                return HttpNotFound();
            }
            ViewBag.eventID = new SelectList(db.Event, "eventID", "Name", log.eventID);
            ViewBag.orgID = new SelectList(db.Org, "orgID", "orgName", log.orgID);
            ViewBag.userId = new SelectList(db.UserProfiles, "UserId", "UserName", log.userId);
            return View(log);
        }

        //
        // POST: /Log/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Log log)
        {
            if (ModelState.IsValid)
            {
                db.Entry(log).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.eventID = new SelectList(db.Event, "eventID", "Name", log.eventID);
            ViewBag.orgID = new SelectList(db.Org, "orgID", "orgName", log.orgID);
            ViewBag.userId = new SelectList(db.UserProfiles, "UserId", "UserName", log.userId);
            return View(log);
        }

        //
        // GET: /Log/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Log log = db.Log.Find(id);
            if (log == null)
            {
                return HttpNotFound();
            }
            return View(log);
        }

        //
        // POST: /Log/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Log log = db.Log.Find(id);
            db.Log.Remove(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}