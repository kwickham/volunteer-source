﻿(function () {
    // Localize jQuery variable
    var jQuery; function getUrlVars() { var vars = {}; var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) { vars[key] = value; }); return vars; }
    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.9.1') {
        var script_tag = document.createElement('script'); script_tag.setAttribute("type", "text/javascript"); script_tag.setAttribute("src", "http://code.jquery.com/jquery-1.7.1.min.js"); if (script_tag.readyState) {
            script_tag.onreadystatechange = function () { // For old versions of IE
                if (this.readyState == 'complete' || this.readyState == 'loaded') { scriptLoadHandler(); }
            };
        } else { script_tag.onload = scriptLoadHandler; }
        // Try to find the head, otherwise default to the documentElement
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery; main();
    }
    /******** Called once jQuery has loaded ******/
    function scriptLoadHandler() {
        // Restore $ and window.jQuery to their previous values and store the
        // new jQuery in our local jQuery variable
        jQuery = window.jQuery.noConflict(true);
        // Call our main function
        main();
    }


    function getXML() {
    $.ajax({
        type: "GET",
        url: "/api/log",
        dataType: "xml",
        success: function (xml) {
            alert($(xml).find('City').text());
        },
        error: function (xml) {
            alert(xml.status + ' ' + xml.statusText);
        }
    });
        $(xml).find("Tutorial").each(function () {
            $(this).attr("author");
        });
    }
    /******** Our main function ********/
    var prefix = getUrlVars()["hostLoc"];
    if (prefix === undefined)
        prefix = '';

    function main() {
        jQuery(document).ready(function ($) {
            /******* Load CSS *******/
            var css_link = $("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: prefix + "/widget/style.css"
            });
            css_link.appendTo('head');

            /******* Load HTML *******/
            myhtml = '<div class="my50-widget-container"><div class="my50-top"></div><div class="my50-content"><div class="status">' +
      'Hello Jim, You\'ve volunteered 12 Hours!' +
    '</div><div class="progress">' +
      '50% Complete' +
'<div class="meter orange"><span style="width: 50%"></span></div></div></div><div class="my50-links"><div class="social-links">' +
      '<div id="fb"><a atl="need image" href="#" class="my50-social-media" ><img class="my50-social-media" src="http://afmarcom.com/blog/wp-content/uploads/2010/12/Facebook-Share-Button.png" height="35px" alt="Facebook"/></a></div>' +
 '<div id="tw"><a class="my50-social-media" atl="need image" href="#"><img src="http://www.mequoda.com/wp-content/uploads/Picture-7.png" height="30px" class="my50-social-media" alt="Twitter" /></a></div></div>' +
    '<a href="" >' +
      '<img class="volunteer-pic"  src="https://raw.github.com/kwickham/volunteer-source/master/Volunteer%20Iowa%20Site/Content/themes/base/images/Volunteer_IA_2color.png" >' +
      '</a></div><div>';

            $('#whats-your-50-container').html(myhtml);
        });
    }
})();