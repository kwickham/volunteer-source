﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Volunteer_Iowa_Site.Models;

namespace Volunteer_Iowa_Site.DAL
{
    public class LogContext : DbContext
    {

        public LogContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Address> Address { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<County> County { get; set; }
        public DbSet<Event> Event { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<Orginization> Org { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
    
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            /*modelBuilder.Entity<Instructor>()
                .HasOptional(p => p.OfficeAssignment).WithRequired(p => p.Instructor);
            modelBuilder.Entity<Course>()
                .HasMany(c => c.Instructors).WithMany(i => i.Courses)
                .Map(t => t.MapLeftKey("CourseID")
                    .MapRightKey("PersonID")
                    .ToTable("CourseInstructor"));
            modelBuilder.Entity<Department>()
                .HasOptional(x => x.Administrator);*/
        }
    }
}