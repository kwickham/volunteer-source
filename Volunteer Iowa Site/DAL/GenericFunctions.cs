﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Volunteer_Iowa_Site.Models;
using Volunteer_Iowa_Site.DAL;
using System.Collections;
using WebMatrix.WebData;

namespace Volunteer_Iowa_Site.DAL
{
    public class GenericFunctions
    {
        private LogContext db = new LogContext();

        public int GetUserID()
        {
            return WebSecurity.CurrentUserId;
        }

        public int GetUserName ( string name )
        {
            return db.UserProfiles.SqlQuery("SELECT * FROM UserProfile WHERE UserName = '" + name + "'" ).Single().UserId;
        }
    }
}