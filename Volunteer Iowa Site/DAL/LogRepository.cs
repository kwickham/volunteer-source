﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Volunteer_Iowa_Site.Models;

namespace Volunteer_Iowa_Site.DAL
{
    public class LogRepository
    {
        private LogContext db;

        public LogRepository(LogContext db)
        {
            this.db = db;
        }

        public IEnumerable<Log> getLog()
        {
            return db.Log.ToList();
        }
        /*
        public Student GetStudentByID(int id)
        {
            return context.Students.Find(id);
        }

        public void InsertEvent(Event Event)
        {
            db.Event.Add(Event);
        }

        public void DeleteStudent(int studentID)
        {
            Student student = context.Students.Find(studentID);
            context.Students.Remove(student);
        }

        public void UpdateStudent(Student student)
        {
            context.Entry(student).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
         
* */
    }
}