﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Volunteer_Iowa_Site.Models;
using Volunteer_Iowa_Site.DAL;

namespace Volunteer_Iowa_Site.API
{
    public class LogController : ApiController
    {
        private LogContext db = new LogContext();

        // GET api/Log
        public IEnumerable<Log> GetLogs()
        {
            var log = db.Log.Include(l => l.Event).Include(l => l.Orginization).Include(l => l.UserProfile);
            return log.AsEnumerable();
        }

        // GET api/Log/5
        public Log GetLog(int id)
        {
            Log log = db.Log.Find(id);
            if (log == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return log;
        }

        // PUT api/Log/5
        public HttpResponseMessage PutLog(int id, Log log)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != log.logID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(log).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/Log
        public HttpResponseMessage PostLog(Log log)
        {
            if (ModelState.IsValid)
            {
                db.Log.Add(log);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, log);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = log.logID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Log/5
        public HttpResponseMessage DeleteLog(int id)
        {
            Log log = db.Log.Find(id);
            if (log == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Log.Remove(log);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, log);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}