﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Volunteer_Iowa_Site.Models
{
    public class Orginization
    {
        [Key]
        [DataType(DataType.Text)]
        [Display(Name = "Org ID")]
        public int orgID { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Organization")]
        public string orgName { get; set; }
    }
}