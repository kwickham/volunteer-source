﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Volunteer_Iowa_Site.Models
{
    public class Event
    {
        [Key]
        public int eventID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Event")]
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        public DateTime startDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "End Date")]
        public DateTime endDate { get; set; }

        [DataType(DataType.Time)]
        [Display(Name = "Start Time")]
        public DateTime startTime { get; set; }

        [DataType(DataType.Time)]
        [Display(Name = "End Time")]
        public DateTime endTime { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Event Details")]
        public string details { get; set; }

        [DataType(DataType.PostalCode)]
        [Display(Name = "Address Id")]
        public int addressID { get; set; }

       /* public virtual Address Address { get; set; }*/
        public virtual Address Address { get; set; }

    }
}