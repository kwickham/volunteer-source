﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Volunteer_Iowa_Site.Models
{
    public class Address
    {
        [Key]
        [DataType(DataType.Text)]
        [Display(Name = "Address ID")]
        public int addressID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Address")]
        public string addressLineOne { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Address Line Two")]
        public string addressLineTwo { get; set; }

        [DataType(DataType.PostalCode)]
        [Display(Name = "Zip")]
        public int addressZip { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "City ID")]
        public int cityId { get; set; }

        //public virtual Address Address { get; set; }
        public virtual City City { get; set; }
    }
}