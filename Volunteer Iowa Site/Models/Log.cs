﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Volunteer_Iowa_Site.Models
{
    public class Log
    {        
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int logID { get; set; }

  

        [Display(Name="Hours")]
        [DataType(DataType.Text)]
        public int hoursLogged { get; set; }

        public int userId { get; set; }

        [Display(Name = "Org Id")]
        public int orgID { get; set; }

        [Display(Name = "Event Id")]
        public int eventID { get; set; }

        public virtual Event Event { get; set; }
        public virtual Orginization Orginization {get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}