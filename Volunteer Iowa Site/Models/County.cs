﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Volunteer_Iowa_Site.Models
{
    public class County
    {
        [Key]
        [DataType(DataType.Text)]
        [Display(Name = "County ID")]
        public int countyID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "County Name")]
        public string countyName { get; set; }
    }
}