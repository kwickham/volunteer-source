﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Volunteer_Iowa_Site.Models
{
    public class City
    {
        [Key]
        [DataType(DataType.Text)]
        [Display(Name = "City ID")]
        public int cityID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "City Name")]
        public string cityName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "County ID")]
        public int countyID { get; set; }

        public virtual County county { get; set; }
    }
}